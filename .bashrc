# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Don't put duplicate lines into the history
HISTCONTROL=ignoredups:erasedups

# Append to the history file, don't overwrite it
shopt -s histappend

# After each command, append to the history file and reread it
PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"

# For setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=200000

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


alias ls='ls -l --almost-all --color=always --human-readable --indicator-style=slash'
alias grep='grep --binary-files=without-match --color'
alias untar='tar -xvf'
alias g='git'
alias guh='git checkout head && git fetch && git pull && git checkout -'
alias ga='git add -p'
gc() {
    git commit -m "$*"
}
alias n='nvim -p'
alias less='less --RAW-CONTROl-CHARS'
alias viewcsv='column -s, -t -n'
alias gdb='gdb --quiet'
alias groq-gdb='groq-gdb --quiet'
alias compiler-gdb='LOGICAL_TOPOLOGY=~/projects/Groq/Groq/Compiler/C2CTopologies/logical_topology.json BIND_SCHEMAS_DIR=~/projects/Groq/Groq/Bind/schemas/0.2/ groq-gdb --quiet --directory $(git rev-parse --show-toplevel) ~/projects/Groq --args ~/projects/Groq/_result/Groq/Compiler/groq-compiler/bin-unwrapped/groq-compiler.unwrapped'
# alias compiler-run='bake build --local //Groq/Compiler:groq-compiler && bake r //Groq/Compiler:groq-compiler'
alias compiler-run='bake build --local -Lverbose //Groq/Compiler:groq-compiler && LOGICAL_TOPOLOGY=~/projects/Groq/Groq/Compiler/C2CTopologies/logical_topology.json BIND_SCHEMAS_DIR=~/projects/Groq/Groq/Bind/schemas/0.2/ ~/projects/Groq/_result/Groq/Compiler/groq-compiler/bin-unwrapped/groq-compiler.unwrapped'
alias bb='bake build'
alias bbl='bake build --local'
alias bb0='bake build -0'
alias bbl0='bake build --local -0'
alias bt='bake test'
alias gi='git'
alias lf='lint --fix'

export RESULT='/home/iprins/projects/Groq/_result'

# export PS1="\u@\h \w\n >\[$(tput sgr0)\] "
_BOLD='\[\e[1m\]'
_WHITE='\[\e[37m\]'
_MAGENTA='\[\e[35m\]'
_BLINK='\[\e[5m\]'
_CLEAR='\[\e[0m\]'
# The printf has the effect of always starting the prompt on a new line,
# even if the cursor ended up on a weird offset column (eg because we
# printed a file that didn't end with a newline)
export PS1='$(printf "%$((COLUMNS-1))s\r")'"${_BOLD}${_WHITE}\u@\h \w${_CLEAR}\]\n \[${_MAGENTA}>${_BLINK}>${_CLEAR} "


export PATH="/home/iprins/packages:/home/iprins/packages/cmake-3.17.5-Linux-x86_64/bin:/home/iprins/squashfs-root/usr/bin:$PATH"

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Failed attempt to rebind the FZF cd to ctrl-g instead of alt-c
# bind -m vi-command '"\C-z": emacs-editing-mode'
# bind -m vi-insert '"\C-z": emacs-editing-mode'
# bind -m emacs-standard '"\C-z": vi-editing-mode'
# if (( BASH_VERSINFO[0] < 4 )); then
#   bind -m emacs-standard '"\C-g": " \C-b\C-k \C-u`__fzf_cd__`\e\C-e\er\C-m\C-y\C-h\e \C-y\ey\C-x\C-x\C-d"'
#   bind -m vi-command '"\C-g": "\C-z\C-g\C-z"'
#   bind -m vi-insert '"\C-g": "\C-z\C-g\C-z"'
# else
#   bind -m emacs-standard -x '"\C-g": " \C-b\C-k \C-u`__fzf_cd__`\e\C-e\er\C-m\C-y\C-h\e \C-y\ey\C-x\C-x\C-d"'
#   bind -m vi-command -x '"\C-g": "\C-z\C-g\C-z"'
#   bind -m vi-insert -x '"\C-g": "\C-z\C-g\C-z"'
# fi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/iprins/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/iprins/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/iprins/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/iprins/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

