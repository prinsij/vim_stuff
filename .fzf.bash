# Setup fzf
# ---------
if [[ ! "$PATH" == */home/iprins/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/iprins/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/iprins/.fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/iprins/.fzf/shell/key-bindings.bash"
