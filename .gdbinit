set history save on
set history size 1000
set history remove-duplicates unlimited
set history filename ~/.gdb_history
set print pretty
set output-radix 10
set print elements 2000
# don't print the program counter for each backtrace line 
set print address off
# Print the actual (derived) type of objects with pointers using vtable
set print object on
set height 0
# print 30 context lines when using 'list'
set listsize 30
set disassembly-flavor intel
# source ~/.gdb/gdb-colour-filter/colour_filter.py
set print frame-arguments scalar
set auto-load safe-path /
