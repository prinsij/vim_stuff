" Language: Alan Assembly
" Maintainer: Jon Purdy <jpurdy@groq.com>

" Don't override an existing syntax setting
if exists("b:current_syntax")
  finish
endif

" Match case-insensitively
syntax case ignore

syntax region alanAsmComment start="//" end="$" contains=alanAsmTodo

syntax match alanAsmIdentifier /[a-z_][0-9A-Za-z_]*/

syntax match alanAsmStream /S(G[0-9]+)?_[0-9]+(_(e(ast(ward)?)?|w(est(ward)?)?|in|out))?/

syntax keyword alanAsmPragma
    \ data
    \ down
    \ dp
    \ import
    \ input
    \ output
    \ read
    \ tid
    \ unit
    \ up
    \ write

syntax keyword alanAsmMnemonic
    \ abc
    \ acc
    \ addmod
    \ addsat
    \ and
    \ bcast
    \ cfg
    \ cfg_pow
    \ cmp_eq
    \ cmp_ge
    \ cmp_gt
    \ cmp_le
    \ cmp_lt
    \ cmp_ne
    \ conv
    \ count
    \ deepsleep
    \ delay
    \ dist
    \ exp2
    \ flush
    \ full_shift319
    \ gather
    \ hold
    \ if
    \ int
    \ iters
    \ iw
    \ log2
    \ lwb
    \ mask
    \ maskbar
    \ max
    \ min
    \ mulmod
    \ mulsat
    \ neg
    \ nop
    \ null
    \ or
    \ perm
    \ perm
    \ read
    \ recirc_shift16
    \ recv
    \ rep
    \ rotate3x3
    \ rotate4x4
    \ rsqrt
    \ sbif
    \ sbnop
    \ scatter
    \ seg
    \ sem
    \ shift
    \ shift16
    \ shift271
    \ shift287
    \ shift303
    \ shift319
    \ shift32
    \ shift48
    \ shift64
    \ shift_im
    \ shift_l
    \ shift_r
    \ shift_ra
    \ step
    \ submod
    \ subsat
    \ tanh
    \ trans1
    \ trans16
    \ trans4
    \ wait
    \ write
    \ xmit
    \ xor
    \ zero

syntax keyword alanAsmKeyword
    \ abme
    \ abre
    \ as0
    \ as1
    \ asre0
    \ asre1
    \ b
    \ b64
    \ bool
    \ captured
    \ clear
    \ coarse
    \ complete
    \ cont
    \ disable
    \ e
    \ east
    \ eastward
    \ enable
    \ f16
    \ f32
    \ fine
    \ float
    \ float16
    \ float32
    \ i16
    \ i32
    \ i8
    \ identity
    \ in
    \ init
    \ init
    \ int16
    \ int32
    \ int8
    \ latch
    \ limits
    \ lowest
    \ lsb
    \ msb
    \ mxm
    \ new
    \ north
    \ notify
    \ old
    \ out
    \ post
    \ recycled
    \ reuse
    \ runtime
    \ sa1
    \ sama0
    \ sama1
    \ sat
    \ select
    \ slt
    \ south
    \ steps
    \ supercell
    \ sync
    \ sync
    \ u16
    \ u32
    \ u8
    \ uint16
    \ uint32
    \ uint8
    \ w
    \ wait
    \ west
    \ westward
    \ wint4
    \ ws0
    \ ws1
    \ zero

syntax keyword alanAsmUnitName
    \ Mem
    \ VXM
    \ Permutor
    \ Selector
    \ Distributor
    \ Transposer
    \ Accumulator
    \ LW
    \ IW
    \ ABC
    \ IO

syntax match alanAsmNumber "\<[0-9]\+\>\|\<0x[0-9a-fA-F]\+\>\|\<0o[0-7]\+\>\|\<0b[01]\+\>\|\<[0-9A-Fa-f]\{2\}\>"

syntax match alanAsmSymbol "[\|]\|,\|:\|=\|+\|-\|@"

syntax keyword alanAsmTodo
    \ FIXME
    \ TODO

syntax keyword alanAsmType
    \ bool
    \ uint8
    \ uint16
    \ uint32
    \ int8
    \ int16
    \ int32
    \ float16
    \ float32

let b:current_syntax = "alanAsm"

highlight default link alanAsmComment    Comment
highlight default link alanAsmIdentifier Identifier
highlight default link alanAsmKeyword    Keyword
highlight default link alanAsmMnemonic   Statement
highlight default link alanAsmNumber     Number
highlight default link alanAsmPragma     PreProc
highlight default link alanAsmStream     Identifier
highlight default link alanAsmSymbol     Operator
highlight default link alanAsmTodo       Todo
highlight default link alanAsmType       Type
highlight default link alanAsmUnitName   Keyword
