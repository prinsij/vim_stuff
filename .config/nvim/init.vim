" ====================
" Options Configuration
" ====================

" Makes Vim not be Vi-compatible. No-op in Nvim but still
set nocompatible
" Load plugin and indent files based on filetype automatically
filetype indent plugin on
" Enable syntax highlighting
syntax on
" Hide buffers instead of unloading them when they are 'abandoned'
set hidden
" Show menu of possible options when trying to complete a : command
set wildmenu
" advance with tab to the longest matching prefix, then stay there
set wildmode=list:longest,longest
" Show partially complete editor command in the bottom right of the screen
set showcmd
" Highlight all matches of search, not just the current
set hlsearch
" Ignore case in search
set ignorecase
" Except if at least one capital used in pattern
set smartcase
" Allow backspace to munch characters it normally couldn't
set backspace=indent,eol,start
" Copy indent from current line when starting a new one
set autoindent
" Add an extra indent when the syntax suggests you should
set smartindent
" Don't move to the start of the current line after executing some commands
set nostartofline
" Use =>  to indicate line continuation
set showbreak==>
" Show line & column #, and relative % in bottom right
set ruler
" Last window should always have a status line
set laststatus=2
" Always use the confirm dialog when possible (like :q w/ changes)
set confirm 
" Disable the annoying 'bell' sound for errors
set t_vb=
" Flash the screen when the bell would play instead
" set visualbell
" Make the command-line at the bottom 2-high to avoid hitting enter so often
set cmdheight=2
" Preceed each line w/ its number
" set number
" Show all line numbers except the current relative to the current
" set relativenumber
" Wait an arbitrary amount of time for mapped sequences to complete
" but only 200ms for a key-code sequence
set notimeout ttimeoutlen=200
" Flip s//g so its global by default, and 1st only by opt-in
set gdefault
" Don't wrap long lines
set nowrap
" If we do put wrap on, break on spaces, punctuation, etc, not middle of words
set linebreak
" Minimal number of screen lines to keep above and below the cursor
set scrolloff=2
" The minimal number of screen columns to keep to the left and to the
" right of the cursor if 'nowrap' is set.
set sidescrolloff=5
" The minimal number of columns to scroll horizontally.  Used only when
" the 'wrap' option is off and the cursor is moved off of the screen.
set sidescroll=1
" Don't append an EOL at the end of files that don't have one
set nofixeol
" Use spaces when you press <tab> in insert mode, use '<>' and w/ autoindent
" Does not apply to pasting
set expandtab
" Use 4 spaces for '<>'
set shiftwidth=4
" Display tabs as 4 long, replace them with 4 spaces
set tabstop=4
" Number of space a <Tab> counts for while editing
set softtabstop=4
" Show where a search will end up while you're still typing it
set incsearch
" Control how insert mode completion will be shown/behave
set completeopt=longest,menuone
" Enable mouse controls in all modes
set mouse=a
" Highlight the cursor's row using CursorLine
" set cursorline
" Show tab characters
set list
set listchars=tab:>-
" This is a list of directories which will be searched when using the
" gf, [f, ]f, ^Wf, :find, :sfind, :tabfind and other commands.
" Launch directory + recursive
set path=.,,**
" Controls the behavior when switching between buffers.
" Jump to any open window in any tab with the target buffer
set switchbuf=usetab
" When a file has been detected to have been changed outside of Vim and
" it has not been changed inside of Vim, automatically read it again.
set autoread
" Where to save swap files and backups of edits
set backupdir=~/.vim_cache
set directory=~/.vim_cache
" Respect indentation when wrapping
" Not supported before Vim 8
if v:version >= 800
  try
    set breakindent
  catch
  endtry
endif
" Do not save local/global values for sessions
set sessionoptions-=options
" https://github.com/airblade/vim-gitgutter
" vim-gitgutter wants this
" it controls the # of milliseconds before git flushes changes to swap
set updatetime=100
" Ignore these useless files when completing things
set wildignore+=*.o,*.pyc,*.d
" Default to C syntax if we can't make up our minds
" set syntax=c
" Open file read-only if a swap-file exists for it
autocmd SwapExists * let v:swapchoice = "o"

" Close all buffers that are hidden (not currently in a window)

function! DeleteHiddenBuffers()
  let tpbl=[]
  let closed = 0
  call map(range(1, tabpagenr('$')), 'extend(tpbl, tabpagebuflist(v:val))')
  for buf in filter(range(1, bufnr('$')), 'bufexists(v:val) && index(tpbl, v:val)==-1')
    if getbufvar(buf, '&mod') == 0
      silent execute 'bwipeout' buf
      let closed += 1
    endif
  endfor
  echo "Closed ".closed." hidden buffers"
endfunction

" Shorter alias for above
command! Bufo call DeleteHiddenBuffers()

" ====================
" Remappings
" ====================

" Capital X -> 8 * X
nnoremap J 8j
vnoremap J 8j
nnoremap K 8k
vnoremap K 8k
" Tab switching
noremap H gT
noremap L gt
" Split switching
noremap <Tab> <C-w>
noremap <C-w> <Nop>
" Remap C-p to go forward (reverse C-o) because the default
" of C-i is used for the tab key by terminals
noremap <C-p> <C-i>
" Spam esc whenever we press esc
inoremap <ESC> <ESC><ESC><ESC><ESC>
vnoremap <ESC> <ESC><ESC><ESC><ESC>
" Remap F1 to Escape instead of calling up the help menu bc
" that is a very frequent and obnoxious fat-finger mistake.
" Just use :help if you want it, it doesn't need a shortcut key.
nnoremap <F1> <Esc>
inoremap <F1> <Esc>
" Move the screen up or down
noremap <C-j> 8<C-e>
noremap <C-k> 8<C-y>
" Horizontally center cursor position
nnoremap z. :<C-u>normal! zszH<Cr>
" Search for current visual selection
vnoremap // y/\V<C-R>"<CR>
" Clear highlight with ctrl-l in normal mode
nnoremap <C-l> :noh<return><C-L>
" Write the file out (like ZZ but don't quit)
nnoremap ZC :update<CR>
" Don't put random control characters when we scroll
" inside insert mode
inoremap <ScrollUp> <nop>
inoremap <ScrollDown> <nop>
inoremap <S-ScrollUp> <nop>
inoremap <S-ScrollDown> <nop>
inoremap <M-ScrollUp> <nop>
inoremap <M-ScrollDown> <nop>
inoremap <C-ScrollUp> <nop>
inoremap <C-ScrollDown> <nop>
" Strip trailing whitespace
nnoremap <F6> :let _s=@/<bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>
" Toggle paste mode with shortcut
" set pastetoggle=<F2>
set pastetoggle=<leader>=
" Control-scroll changes font size
noremap <C-ScrollWheelUp> :call AdjustFontSize(1)<CR>
noremap <C-ScrollWheelDown> :call AdjustFontSize(-1)<CR>
inoremap <C-ScrollWheelUp> <Esc>:call AdjustFontSize(1)<CR>a
inoremap <C-ScrollWheelDown> <Esc>:call AdjustFontSize(-1)<CR>a
" " change font size with Ctrl + mouse-wheel up/down
" " only works in gui mode
" let s:fontsize = 14
" function! AdjustFontSize(amount)
"   let s:fontsize = s:fontsize+a:amount
"   :execute "GuiFont! Consolas:h" . s:fontsize
" endfunction

" ====================
" Syntax Support
" ====================

autocmd FileType python setlocal shiftwidth=4 tabstop=4 softtabstop=4
autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType css setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType nim setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType cpp setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType cmake setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd Filetype alan-assembly setlocal expandtab tabstop=8 shiftwidth=8 softtabstop=8
autocmd Filetype nix setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2

" Disable syntax for large files
autocmd BufWinEnter * if line2byte(line("$") + 1) > 1000000 | syntax clear | endif
" Syntax/indents for various languages
autocmd BufNewFile,BufRead,BufEnter *.cpp,*.hpp set omnifunc=omni#cpp#completeMain
autocmd BufNewFile,BufRead,BufEnter *.txt setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd BufNewFile,BufRead,BufEnter *.hs setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd BufNewFile,BufRead,BufEnter *.bazel set syntax=python
autocmd BufNewFile,BufRead,BufEnter *.aa set filetype=alan-assembly
autocmd BufNewFile,BufRead,BufEnter *.nix set filetype=nix
" Because CMake is weird and doesn't have a file extension
autocmd BufRead,BufNewFile .cmake,CMakeLists.txt,.cmake.in runtime! indent/cmake.vim
autocmd BufRead,BufNewFile .cmake,CMakeLists.txt,.cmake.in setf cmake
autocmd BufRead,BufNewFile .ctest,.ctest.in setf cmake


"autocmd VimEnter * tabnew ~/notes/universal.txt
let g:semanticEnableFileTypes = [] "['*.cpp', '*.py', '*.h', '*.cl']

" ====================
" Visuals
" ====================

" Allow color schemes to do bright colors without forcing bold.
if &t_Co == 8 && $TERM !~# '^Eterm'
  set t_Co=16
endif

colorscheme ron

hi WarningMsg ctermfg=white ctermbg=red guifg=White guibg=Red gui=None
hi Cursor ctermfg=black ctermbg=green cterm=bold guifg=black guibg=green gui=bold

highlight Search ctermfg=magenta ctermbg=black cterm=underline
highlight IncSearch ctermfg=magenta ctermbg=black cterm=underline

" notably the line continuation symbol
highlight NonText ctermfg=magenta ctermbg=black guifg=magenta guibg=black

" highlight CursorLine term=bold cterm=bold guibg=black ctermbg=black
" highlight CursorLine term=None cterm=none guibg=black ctermbg=black
highlight CursorLine term=NONE cterm=NONE guifg=fg ctermbg=232 guibg=080808
" highlight CursorLine term=None cterm=none ctermbg=16 guibg=#000000

" default todo highlighting is hidden by cursorline
highlight Todo ctermfg=green ctermbg=black guifg=green guibg=black cterm=bold gui=bold

" ====================
" Leader Mappings
" ====================

" map the leader key to space
let mapleader = (' ')

" t for new tab
noremap <leader>t :tabnew 

" h for semantic highlight
noremap <leader>h :SemanticHighlightToggle<return>

" s for switch to buffer name
noremap <leader>s :tab sbuf 

" fzf a file
nnoremap <leader>f :Files<return>
" fzf a string
nnoremap <leader>g :RG<return>
" fzf a buffer
nnoremap <leader>b :Buffers<return>

" l for lint repo
nnoremap <leader>l :!lint --fix
" " l for lint current file
" nnoremap <leader>l :!clang-format --style=file -i %:p<return>:e<return>

" -/'+' to move tab to the left/right
nnoremap <leader>- :tabmove -1<return>
nnoremap <leader>= :tabmove +1<return>

" Close all tabs in the direction pointed
nnoremap <leader>> :.+1,$tabdo :tabc<return>
nnoremap <leader>< :.-1,$tabdo :tabc<return>

" Yank the current selection into the tmux clipboard buffer
" https://github.com/tpope/vim-tbone/issues/11#issuecomment-780939888
vnoremap <leader>y "zy:tabnew<CR>"zP:w !xargs -0 tmux set-buffer<CR><CR>:bdelete!<CR>

" ====================
" FZF Config
" ====================

" https://github.com/junegunn/fzf/blob/master/README-VIM.md
set runtimepath+=~/.fzf

" 'advanced' ripgrep integration
" ie re-grep with every changed character, instead of the wonky default behavior
function! RipgrepFzf(query, fullscreen)
  let command_fmt = 'rg --column --line-number --no-heading --fixed-strings --color=always --smart-case -- %s || true'
  let initial_command = printf(command_fmt, shellescape(a:query))
  let reload_command = printf(command_fmt, '{q}')
  let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
  call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction

command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)

function! s:build_quickfix_list(lines)
  call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
  copen
  cc
endfunction

let g:fzf_action = {
  \ 'ctrl-q': function('s:build_quickfix_list'),
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

