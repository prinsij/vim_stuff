# remap prefix from 'C-b' to 'C-f'
unbind C-b
set-option -g prefix C-f
bind-key C-f send-prefix

# split panes using | and %
bind | split-window -h
bind - split-window -v
unbind '"'
unbind '%'

# reload config file
bind r source-file ~/.tmux.conf

# switch panes using Alt-hjkl without prefix
bind -n M-h select-pane -L
bind -n M-l select-pane -R
bind -n M-k select-pane -U
bind -n M-j select-pane -D

# Enable mouse support for scrolling, selecting windows, etc
# However this switches the click-n-drag selecting to tmux buffer
set -g mouse on

# Fix latency pressing escape
set -sg escape-time 0

# Easily enter copy mode
# unbind [
# bind -n C-J copy-mode

# Easily switch to last window
bind -n C-l last-window
bind -n C-s last-window

# Use prefix-m to enable scrolling
# and shift to turn it off again
bind m set-window-option -g mouse on
bind M set-window-option -g mouse off

##### https://github.com/tmux/tmux/issues/140#issuecomment-474341833

# disable "release mouse drag to copy and exit copy-mode", ref: https://github.com/tmux/tmux/issues/140
unbind-key -T copy-mode-vi MouseDragEnd1Pane

# since MouseDragEnd1Pane neither exit copy-mode nor clear selection now,
# let single click do selection clearing for us.
bind-key -T copy-mode-vi MouseDown1Pane select-pane\; send-keys -X clear-selection

# this line changes the default binding of MouseDrag1Pane, the only difference
# is that we use `copy-mode -eM` instead of `copy-mode -M`, so that WheelDownPane
# can trigger copy-mode to exit when copy-mode is entered by MouseDrag1Pane
bind -n MouseDrag1Pane if -Ft= '#{mouse_any_flag}' 'if -Ft= \"#{pane_in_mode}\" \"copy-mode -eM\" \"send-keys -M\"' 'copy-mode -eM'
